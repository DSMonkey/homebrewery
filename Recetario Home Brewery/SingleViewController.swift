//
//  SingleViewController.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 27/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class SingleViewController: UIViewController {
   
    @IBOutlet var tableView: UITableView!

    var recipes :[Recipe] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*var recipe = Recipe(name: "IPA Export",
                            image : #imageLiteral(resourceName: "craft-beer"),
                            descripcion : "Muy buena IPA americana",
                            litros : "23",
                            APV : 10,
                            amargor : 15,
                            color : 25,
                            DO : 1052,
                            DF : 1014,
                            time : 60,
                            ingredients : ["cebada", "Lupulo", "Agua"],
                            steps : ["hervir","colar","Fermentar"],
                            procedimiento : ["paso 1", "paso 2", "paso 3"]
        )
        recipes.append(recipe)
        recipe = Recipe(name: "STOUT",
                        image : #imageLiteral(resourceName: "craft-beer"),
                        descripcion : "Muy buena IPA americana",
                        litros : "23",
                        APV : 10,
                        amargor : 15,
                        color : 25,
                        DO : 1052,
                        DF : 1014,
                        time : 60,
                        ingredients : ["cebada", "Lupulo", "Agua"],
                        steps : ["hervir","colar","Fermentar"],
                        procedimiento : ["paso 1", "paso 2", "paso 3"]
        )
        recipes.append(recipe)
        recipe = Recipe(name: "Pale Ale",
                        image : #imageLiteral(resourceName: "craft-beer"),
                        descripcion : "Muy buena IPA americana",
                        litros : "23",
                        APV : 10,
                        amargor : 15,
                        color : 25,
                        DO : 1052,
                        DF : 1014,
                        time : 60,
                        ingredients : ["cebada", "Lupulo", "Agua"],
                        steps : ["hervir","colar","Fermentar"],
                        procedimiento : ["paso 1", "paso 2", "paso 3"]
        )
        
        recipes.append(recipe)*/

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SingleViewController : UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let recipe = recipes[indexPath.row]
        let cellID = "RecipeCell"
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! SingleRecipeCell
        
       

       
        cell.Image1.image = recipe.image
        cell.labelName.text = recipe.name
        cell.labelLitros.text = "Para \(recipe.litros!) litros de cerveza"
        
        cell.Image1.layer.cornerRadius = 42.0
        cell.Image1.clipsToBounds = true
       
        return cell
    }
    
}
