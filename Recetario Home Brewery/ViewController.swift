//
//  ViewController.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 23/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class ViewController: UITableViewController{
    
    var recipes :[Recipe] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        
        var recipe = Recipe(name: "Brown Porter",
                            tipo: "Extracto de malta",
                            image : #imageLiteral(resourceName: "BrownPorter"),
                            descripcion : "Una cerveza de carácter tostado con notas de chocolate y caramelo",
                            litros : "23",
                            APV : "10",
                            amargor : "15",
                            color : "25",
                            DO : "1052",
                            DF : "1014",
                           
                            
                            maltas : ["3,3 kg de extracto de malta claro seco", "350 g de malta cristal oscura", "200 g de malta chocolate", "30 g de malta marrón"],
                            levaduras : ["Wyeast 1028 London Ale", "46 g  de First Gold 8 %"],
                            lupulos : ["46 g  de First Gold 8 %"],
                            otros : ["1 cucharadita de Irish moss"],
                            
                            
                            procedimiento : ["Remojar la malta cristal oscura, la malta chocolate y la malta marrón en 27 litros de agua a 65 ºC durante 30 minutos. ", "Retirar la malta y agregar el extracto de malta claro seco. ", "Llevar a ebullición y dejar en ebullición durante 1 hora y 10 minutos.","Durante ese tiempo añadir los lúpulos y el clarificante.","31 g de First Gold 8 % al inicio de la ebullición. ","El Irish moss 15 minutos antes de finalizar.","15 g de First Gold 8 % 10 minutos antes de finalizar.","Inocular la levadura y dejar fermentar a 18 ºC.","Dejar acondicionar 4 semanas a 12 ºC."]
                            )
         recipes.append(recipe)
        recipe = Recipe(name: "Brown Porter",
                        tipo: "Extracto de malta",
                        image : #imageLiteral(resourceName: "rubia"),
                        descripcion : "Una cerveza de carácter tostado con notas de chocolate y caramelo",
                        litros : "23",
                        APV : "10",
                        amargor : "15",
                        color : "25",
                        DO : "1052",
                        DF : "1014",
                        
                        
                        maltas : ["3,3 kg de extracto de malta claro seco", "350 g de malta cristal oscura", "200 g de malta chocolate", "30 g de malta marrón"],
                        levaduras : ["Wyeast 1028 London Ale", "46 g  de First Gold 8 %"],
                        lupulos : ["46 g  de First Gold 8 %"],
                        otros : ["1 cucharadita de Irish moss"],
                        
                        
                        procedimiento : ["Remojar la malta cristal oscura, la malta chocolate y la malta marrón en 27 litros de agua a 65 ºC durante 30 minutos. ", "Retirar la malta y agregar el extracto de malta claro seco. ", "Llevar a ebullición y dejar en ebullición durante 1 hora y 10 minutos.","Durante ese tiempo añadir los lúpulos y el clarificante.","31 g de First Gold 8 % al inicio de la ebullición. ","El Irish moss 15 minutos antes de finalizar.","15 g de First Gold 8 % 10 minutos antes de finalizar.","Inocular la levadura y dejar fermentar a 18 ºC.","Dejar acondicionar 4 semanas a 12 ºC."]
        )
         recipes.append(recipe)
        recipe = Recipe(name: "Brown Porter",
                        tipo: "Extracto de malta",
                        image : #imageLiteral(resourceName: "summer"),
                        descripcion : "Una cerveza de carácter tostado con notas de chocolate y caramelo",
                        litros : "23",
                        APV : "10",
                        amargor : "15",
                        color : "25",
                        DO : "1052",
                        DF : "1014",
                        
                        
                        maltas : ["3,3 kg de extracto de malta claro seco", "350 g de malta cristal oscura", "200 g de malta chocolate", "30 g de malta marrón"],
                        levaduras : ["Wyeast 1028 London Ale", "46 g  de First Gold 8 %"],
                        lupulos : ["46 g  de First Gold 8 %"],
                        otros : ["1 cucharadita de Irish moss"],
                        
                        
                        procedimiento : ["Remojar la malta cristal oscura, la malta chocolate y la malta marrón en 27 litros de agua a 65 ºC durante 30 minutos. ", "Retirar la malta y agregar el extracto de malta claro seco. ", "Llevar a ebullición y dejar en ebullición durante 1 hora y 10 minutos.","Durante ese tiempo añadir los lúpulos y el clarificante.","31 g de First Gold 8 % al inicio de la ebullición. ","El Irish moss 15 minutos antes de finalizar.","15 g de First Gold 8 % 10 minutos antes de finalizar.","Inocular la levadura y dejar fermentar a 18 ºC.","Dejar acondicionar 4 semanas a 12 ºC."]
        )
        recipes.append(recipe)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnSwipe = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated....
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    @IBAction func closePrincipal(segue: UIStoryboardSegue){
        
    }
        
    

    //MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let recipe = recipes[indexPath.row]
        let cellID = "RecipeCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! RecipeCell
        
        cell.thumbnailImageView.image = recipe.image
        cell.nameLabel.text = recipe.name
        cell.LitrosLabel.text = "Para \(recipe.litros!) litros de cerveza"
        
        cell.thumbnailImageView.layer.cornerRadius = 42.0
        cell.thumbnailImageView.clipsToBounds = true
        cell.thumbnailImageView.layer.borderWidth = 1
        cell.thumbnailImageView.layer.borderColor = (UIColor.white).cgColor
        
        return cell
    }
    
    // añadir funcionalidad de borrar el deslizar
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{ //Si el esiting style = delete borra la celda que se encuantra en el indexpath.row
            
            self.recipes.remove(at: indexPath.row)
            
        }
        self.tableView.deleteRows(at: [indexPath], with: .fade) //Recarga la tabla para respetar el Modelo - Vista -  Controlador
    }
     //Añadir mas de una accion al deslizar***********
    
        // Compartir......
      override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
            
            let shareAction = UITableViewRowAction(style: .default, title: "Compartir") { (Action, indexPath) in
                let shareDefaultText = "Estoy mirando la receta \(self.recipes[indexPath.row].name!) en la aplicación de recetas fantasticas del curso de Swift 3"
                
                let activityController = UIActivityViewController(activityItems: [shareDefaultText, self.recipes[indexPath.row].image], applicationActivities: nil)
                
                self.present(activityController, animated: true, completion: nil)
            }
            // Establece el color del boton
            shareAction.backgroundColor = UIColor(colorLiteralRed: 30.0/255.0, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0)
            
            // Borrar .....
            
            let deleteAction = UITableViewRowAction(style: .default, title: "Borrar") { (action, indexPath) in
                self.recipes.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
            // Establece el color del boton
            deleteAction.backgroundColor = UIColor(colorLiteralRed: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
            
            return [deleteAction, shareAction]
        }
    
    //MARK: UITableViewDelagate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
    }
    
    // Envio de la receta al view controller de destino a traves del Segue.
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        //comprobación del identificador del segue
        if segue.identifier == "showRecipeDetail"{
            
            // Casting seguro para comprobar que se a pulsado una celda
            if let indexpath = self.tableView.indexPathForSelectedRow{
                //Obtenemos el indexpath de la receta seleccionada
                let selectedRecipe = self.recipes[indexpath.row]
                // asignamos el controlador de destino con un casting al controlador que controla la vista destino
                let destinationViewController = segue.destination as! DetailViewController
                //Le pasamos la receta a la variable de tipo Recipe de la vista destino.
                destinationViewController.recipe = selectedRecipe
                
            }
        }
    }



}

