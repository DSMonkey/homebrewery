//
//  RecipeCell.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 25/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {
    
 
    @IBOutlet var thumbnailImageView: UIImageView!
    
    @IBOutlet var nameLabel: UILabel!

    @IBOutlet var LitrosLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
