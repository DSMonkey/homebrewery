//
//  DetailViewController.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 29/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
   
    @IBOutlet var recipeImageView: UIImageView!
    
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var ratingButton: UIButton!
    
    var recipe : Recipe!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        self.title = recipe.name
        self.recipeImageView.image = self.recipe.image
      
        self.tableView.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)// Elimina lo que viene despues de la ultima celda
        self.tableView.separatorColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.75)
        

        
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        let image = UIImage(named: self.recipe.rating )
        self.ratingButton.setImage(image, for: .normal)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setToolbarHidden(false, animated: true)
        
    }
    
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    
    @IBAction func close(segue: UIStoryboardSegue){
        
        if let reviewVC = segue.source as? ReviewViewController{
            
            if let rating = reviewVC.ratingSelected{
                self.recipe.rating = rating
                let image = UIImage(named: self.recipe.rating )
                self.ratingButton.setImage(image, for: .normal)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DetailViewController : UITableViewDataSource{
    
    //Numero de secciones o bloques
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    //Numero de filas en cada bloque (Aplicamos un Switch para hacerlo dinamico)
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 9 // En el caso de que el numero de seccion de 0 definimos dos filas (Para el texto "Nombre" y para el nombre propio)
        case 1:
            return self.recipe.maltas.count // En el caso de que el numero de seccion de 1 tantas filas como el total de registros de ingredientes
        case 2:
            return self.recipe.levaduras.count // En el caso de que el numero de seccion de 1 tantas filas como el total de registros de ingredientes
        case 3:
            return self.recipe.lupulos.count // En el caso de que el numero de seccion de 1 tantas filas como el total de registros de ingredientes
        case 4:
            return self.recipe.otros.count // En el caso de que el numero de seccion de 1 tantas filas como el total de registros de ingredientes
        case 5:
            return self.recipe.procedimiento.count // En el caso de que el numero de seccion de 2 tantas filas como el total de registros de procedimiento
        default: return 0 // Retornamos 0 porque hay que retornar algo o peta
            
        }
    }
    
    // Valores de las celtas por seccion.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailRecipeCell", for: indexPath) as! RecipeDetailViewCell //Casteamos a el controlador de la celda deseada
        
        cell.backgroundColor = UIColor.clear// resetea el color por defecto
        
        switch indexPath.section {
        case 0: // en caso de indexth de la seccion sea 0 (Bloque del nombre y litros) dediante otro swith si el indexpath de la fila es 0
            
            switch indexPath.row {
            case 0: // Si el indexpath.row es 0
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Nombre:" // Le pongo el texto "Nombre"
                cell.valueLabel.text = recipe.name // Le pongo el valor
                
            case 1:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Tipo:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.tipo // Le pongo el valor
                
            case 2: // Si el indexpath.row es 0
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Litros:" // Le pongo el texto "Litros"
                cell.valueLabel.text = "Para \(recipe.litros!) litros de cerveza" // Le pongo el valor
                
            case 3:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Descrip:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.descripcion // Le pongo el valor
            case 4:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "APV:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.APV + "% (Grado alcoholico)"// Le pongo el valor
            case 5:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Amargor:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.amargor + " IBU"// Le pongo el valor
            case 6:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "Color:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.color + " EBC"// Le pongo el valor
            case 7:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "DO:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.DO + " (Densidad original)"// Le pongo el valor
            case 8:
                cell.keyLabel.isHidden = false
                cell.keyLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                cell.keyLabel.textAlignment = NSTextAlignment.left
                cell.keyLabel.text = "DF:" // Le pongo el texto "Litros"
                cell.valueLabel.text = recipe.DF + " (Densidad final)"// Le pongo el valor
            default: break
            }
            
        case 1: // en caso de indexpath de la seccion sea 1 (Ingredientes) mediante un If compruebo si es el indexpath.row es 0 para ponerle el texto del nombre y de lo contrario lo dejo en blanco
              cell.keyLabel.isHidden = false
            let index = self.recipe.maltas[indexPath.row]
            
            if self.recipe.maltas.contains(index){
                
                let indice = self.recipe.maltas.index(of: index)
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -"
                cell.keyLabel.textAlignment = NSTextAlignment.center
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }

            cell.valueLabel.text = self.recipe.maltas[indexPath.row] // Le pongo el valor llamando el indexpath.row porque es un Array
        case 2: // en caso de indexpath de la seccion sea 1 (Ingredientes) mediante un If compruebo si es el indexpath.row es 0 para ponerle el texto del nombre y de lo contrario lo dejo en blanco
             cell.keyLabel.isHidden = false
            let index = self.recipe.levaduras[indexPath.row]
            
            if self.recipe.levaduras.contains(index){
                
                let indice = self.recipe.levaduras.index(of: index)
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -"
                cell.keyLabel.textAlignment = NSTextAlignment.center
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }

            
            
            cell.valueLabel.text = self.recipe.levaduras[indexPath.row] // Le pongo el valor llamando el indexpath.row porque es un Array
        case 3: // en caso de indexpath de la seccion sea 1 (Ingredientes) mediante un If compruebo si es el indexpath.row es 0 para ponerle el texto del nombre y de lo contrario lo dejo en blanco
             cell.keyLabel.isHidden = false
            let index = self.recipe.lupulos[indexPath.row]
            
            if self.recipe.lupulos.contains(index){
                
                let indice = self.recipe.lupulos.index(of: index)
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -"
                cell.keyLabel.textAlignment = NSTextAlignment.center
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }

            
            
            cell.valueLabel.text = self.recipe.lupulos[indexPath.row] // Le pongo el valor llamando el indexpath.row porque es un Array

        case 4: // en caso de indexpath de la seccion sea 1 (Ingredientes) mediante un If compruebo si es el indexpath.row es 0 para ponerle el texto del nombre y de lo contrario lo dejo en blanco
            cell.keyLabel.isHidden = false
            let index = self.recipe.otros[indexPath.row]
            
            if self.recipe.otros.contains(index){
                
                let indice = self.recipe.otros.index(of: index)
                
                cell.keyLabel.text = String(describing: indice! + 1 ) + " -"
                cell.keyLabel.textAlignment = NSTextAlignment.center
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
            }

            
            
            cell.valueLabel.text = self.recipe.otros[indexPath.row] // Le pongo el valor llamando el indexpath.row porque es un Array

        case 5: // en caso de indexpath de la seccion sea 2 (Procedimiento) mediante un If compruebo si es el indexpath.row es 0 para ponerle el texto del nombre y de lo contrario lo dejo en blanco
            
            
            let index = self.recipe.procedimiento[indexPath.row]
            
            if self.recipe.procedimiento.contains(index){
                
                let indice = self.recipe.procedimiento.index(of: index)
                
                cell.keyLabel.text = "Paso " + String(describing: indice! + 1 )
                cell.keyLabel.textAlignment = NSTextAlignment.center
                cell.keyLabel.textColor = #colorLiteral(red: 0.1200981066, green: 0.5898380876, blue: 1, alpha: 1)
                
                
            }

            
            //cell.keyLabel.isHidden = true
            cell.valueLabel.text = self.recipe.procedimiento[indexPath.row] // Le pongo el valor llamando el indexpath.row porque es un Array
            
            
        default:
            break
        }
        
        return cell // Retorno la celda.
    }
 
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = ""
        
        switch section {
        case 1:
            title = "Maltas: " + String(self.recipe.maltas.count)
        case 2:
            title = "Levaduras: " + String(self.recipe.levaduras.count)
        case 3:
            title = "Lupulos: " + String(self.recipe.lupulos.count)
        case 4:
            title = "Otros: " + String(self.recipe.otros.count)
        case 5:
            title = "Procedimiento: "
      
        default:
            break
        }
        return title
    }
    
    
    
}
