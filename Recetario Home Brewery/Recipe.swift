//
//  Recipe.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 23/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved. 
//

import Foundation
import UIKit

class Recipe: NSObject {
    
    var name : String!
    var tipo : String!
    var image : UIImage! // Para usar el UIImage debemos importar el UIKit
    var descripcion : String!
    var litros : String!
    var APV : String!
    var amargor : String!
    var color : String!
    var DO : String!
    var DF : String!
    var maltas : [String]!
    var levaduras : [String]!
    var lupulos : [String]!
    var otros : [String]!
    var procedimiento : [String]!
    
    var rating : String = "rating"
    
    init(name: String, tipo : String ,image :UIImage, descripcion : String, litros : String,APV : String , amargor : String, color : String,DO : String ,DF : String  ,maltas : [String] , levaduras : [String]!,
    lupulos : [String]!, otros : [String]!, procedimiento : [String]) {
        
        
        self.name = name
        self.tipo = tipo
        self.image = image
        self.descripcion = descripcion
        self.litros = litros
        self.APV = APV
        self.amargor = amargor
        self.color = color
        self.DO = DO
        self.DF = DF
        self.maltas = maltas
        self.levaduras = levaduras
        self.lupulos = lupulos
        self.otros = otros
        self.procedimiento = procedimiento
    }
    
}
