//
//  RecipeDetailViewCell.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 29/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class RecipeDetailViewCell: UITableViewCell {

    
    @IBOutlet var keyLabel: UILabel!
  
    @IBOutlet var valueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
