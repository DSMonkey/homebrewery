//
//  SingleRecipeCell.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 28/7/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class SingleRecipeCell: UITableViewCell {
    
    @IBOutlet var Image1: UIImageView!

    @IBOutlet var labelName: UILabel!
    
    @IBOutlet var labelLitros: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
