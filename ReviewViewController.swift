//
//  ReviewViewController.swift
//  Recetario Home Brewery
//
//  Created by Jesus Navarrete Perez on 3/8/17.
//  Copyright © 2017 dsmonkey. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    
    var ratingSelected :String?

    
    @IBOutlet var backgroundImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let blurEffect = UIBlurEffect(style: .light) // creo una variable y elejo el efecto
        let blurEffectView = UIVisualEffectView(effect: blurEffect) // Le asigno la variable al efecto
        blurEffectView.frame = view.bounds// recorto el efecto a las medidas de la pantalla
        backgroundImageView.addSubview(blurEffectView)// le aplico el efecto como una capa.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func ratingPressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            ratingSelected = "dislike"
        case 2:
            ratingSelected = "good"
        case 3:
            ratingSelected = "great"
        default:
            break
        }
        
        performSegue(withIdentifier: "unwindToDetailView", sender: sender)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
